import { Navigation } from 'react-native-navigation';
import AuthScreen  from './src/screens/AuthScreen';
import HomeScreen from './src/screens/HomeScreen';
import SplashScreen from './src/screens/SplashScreen';

// Register Screens
Navigation.registerComponent("safeelite.SplashScreen",() =>  SplashScreen);
Navigation.registerComponent("safeelite.AuthScreen",() => AuthScreen);
Navigation.registerComponent("safeelite.HomeScreen",() =>  HomeScreen);

// Start a App
Navigation.startSingleScreenApp({
    screen:{
      screen:"safeelite.SplashScreen",
      title:"Splash",
      navigatorStyle: {navBarHidden: true}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
    },
    navigationOptions: {
      header: {
        left: null,
      }
    },
    animationType: 'fade' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
});