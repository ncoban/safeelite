import React, { Component } from 'react';
import { View, Text, BackHandler, StyleSheet, WebView, Dimensions, Platform, Image } from 'react-native';
import * as Keychain from 'react-native-keychain';
import { Button, FormValidationMessage } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Feather';
import ModalDropdown from 'react-native-modal-dropdown';
import Spinner from 'react-native-loading-spinner-overlay';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const API_URL = "https://tracking.safee.xyz/api/v1";

class HomeScreen extends Component {
    constructor(props){
        super(props);
        this.logoutHandler = this.logoutHandler.bind(this);
        this.togglePanelHandler = this.togglePanelHandler.bind(this);
        this.getTokenFromRemote = this.getTokenFromRemote.bind(this);
        this.getSiteFromRemote = this.getSiteFromRemote.bind(this);
        this.getCategoryFromRemote = this.getCategoryFromRemote.bind(this);
        this.getVehicleFromRemoteByCatId = this.getVehicleFromRemoteByCatId.bind(this);
        this.generateQueryString = this.generateQueryString.bind(this);
        this.filterHandler = this.filterHandler.bind(this);

        this.state = {
            loading: false,
            togglePanel: false,
            username:'',
            password:'',
            token:'',
            sites:[],
            sitesDropdown:[],
            categories:[],
            categoriesDropdown:[],
            vehicles:[],
            vehiclesDropdown:[],
            selectedSiteId:-1,
            selectedCategoryId:-1,
            selectedVehicleId:-1,
            queryString:""
        }
    }
    async componentWillMount(){
        // When you have the JWT credentials
        try {
          // Retreive the credentials
          const credentials = await Keychain.getGenericPassword();
          if (credentials) {
              console.log(credentials);
            if(credentials.username == undefined && credentials.password == undefined){
              this.logoutHandler();
            }else{
                this.setState({username:credentials.username,password:credentials.password});
                this.getTokenFromRemote();
            }
          } else {
            this.logoutHandler();
          }
        } catch (error) {
            //console.log("Burada4 ");
            ///this.logoutHandler();
            console.log('Keychain couldn\'t be accessed!', error);
        } 
    }
    getTokenFromRemote(){
        fetch(API_URL + "/auth/login", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
                fcmToken: "foMYWGYk2L0:APA91bGZ...."
            }),
        }).then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if(responseJson.status == "success"){
            // Add Token and Go
            this.setState({token:responseJson.result.authToken});
            this.filterHandler(responseJson.result.authToken);
            // Get Site From Remote
            this.getSiteFromRemote(responseJson.result.authToken);
            // Get Category From Remote
            this.getCategoryFromRemote(responseJson.result.authToken);

          }else{
            this.logoutHandler();
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
    getSiteFromRemote(token){
        fetch(API_URL + "/site/list", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization':'Bearer '+ token
            }
        }).then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if(responseJson.status == "success"){
               let sitesDropdown = responseJson.result.map(x => x.name);
               sitesDropdown = ["All Site",...sitesDropdown];
               this.setState({sitesDropdown:sitesDropdown});
               this.setState({sites:responseJson.result});
               this._dropdown_site_select(0);
          }else{
            this.logoutHandler();
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
    getCategoryFromRemote(token){
        fetch(API_URL + "/category/list", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization':'Bearer '+ token
            }
        }).then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if(responseJson.status == "success"){
               /* const categoriesDropdown = ["All Category"];
               this.setState({categoriesDropdown:categoriesDropdown}); */
               this.setState({categories:responseJson.result});
          }else{
            this.logoutHandler();
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
    getVehicleFromRemoteByCatId(catId){
        fetch(API_URL + "/vehicle/filter", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization':'Bearer '+ this.state.token
            },
            body: JSON.stringify({
                "pageSize": 1000,
                "pageIndex": 0,
                "filter": {
                    "categoryId": catId
                }
            })
        }).then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if(responseJson.status == "success"){
                let vehiclesDropdown = responseJson.result.map(x => x.plateNo);
                vehiclesDropdown = ["All Vehicles",...vehiclesDropdown];
                this.setState({vehiclesDropdown:vehiclesDropdown});
                this.setState({vehicles:responseJson.result});
          }else{
            this.logoutHandler();
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
    generateQueryString(token){
        const siteId = this.state.selectedSiteId;
        const catId = this.state.selectedCategoryId;
        const vehicleId = this.state.selectedVehicleId;
        query = "";
        if(siteId != -1){
            query = "dashboard_fullscreen.xhtml?token="+token+"&level="+siteId;
            if(catId != -1){
                query = "dashboard_fullscreen.xhtml?token="+token+"&level="+catId;
                if(vehicleId != -1){
                    //console.log("v:"+vehicleId,"t:"+token);
                    query = "dashboard-vehicle.xhtml?token="+token+"&id="+vehicleId;
                }
            }
        }else{
            query = "dashboard_fullscreen.xhtml?token="+token;
        }
        console.log(query);
        return query;
    }
    // Handle Back Button
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    handleBackButton() {
        return true;
    }
    async logoutHandler(){
        await Keychain.resetGenericPassword().then(()=>{
            this.props.navigator.push({
                screen: 'safeelite.AuthScreen',
                title: 'Auth Screen',
                headerLeft: null,
                navigatorStyle: {navBarHidden: true}, 
            });
        });
    }
    togglePanelHandler(){
        this.setState({togglePanel:!this.state.togglePanel});
    }
    selectSite(index,text){
        this.setState({loading: true});
        this._dropdown_category_select(0);
        this._dropdown_vehicle_select(0);
        this.setState({selectedCategoryId:-1});
        this.setState({selectedVehicleId:-1});
        this.setState({categoriesDropdown:["All Categories"]});
        this.setState({vehiclesDropdown:["All Vehicles"]});
        if(index == 0){
            this.setState({selectedSiteId:-1});    
            return;
        }

        const siteId = this.state.sites[index-1].id;
        this.setState({selectedSiteId:siteId});

        // Fill Categories With Site Id
        let categoriesDropdown = [];
        this.state.categories.forEach((x) => {
            if(x.siteId === siteId)
                categoriesDropdown.push(x.name);
        });
        categoriesDropdown = ["All Category",...categoriesDropdown];
        this.setState({categoriesDropdown:categoriesDropdown});
        this.setState({loading: false});
    }
    selectCategory(index,text){
        this._dropdown_vehicle_select(0);
        this.setState({selectedVehicleId:-1});
        this.setState({vehiclesDropdown:["All Vehicles"]});
        if(index == 0){
            this.setState({selectedCategoryId:-1});    
            return;
        }

        // Select Categories with Site ID
        let categoriesDropdown = [];
        this.state.categories.forEach((x) => {
            if(x.siteId === this.state.selectedSiteId){
                categoriesDropdown.push(x.id);
            }
        });

        console.log(categoriesDropdown);
        console.log(index);
        const catId = categoriesDropdown[index-1];
        this.setState({selectedCategoryId:catId});
        console.log(catId);
        
        // Fill Categories With Site Id
        this.getVehicleFromRemoteByCatId(catId);
    }
    selectVehicle(index,text){
        if(index == 0){
            this.setState({selectedVehicleId:-1});    
            return;
        }
        const vehicleId = this.state.vehicles[index-1].id;
        this.setState({selectedVehicleId:vehicleId});
    } 
    filterHandler(token){
        this.setState({queryString:this.generateQueryString(token)});
        this.setState({togglePanel:!this.state.togglePanel});
    }
    clearFilterHandler(){
        this.setState({selectedSiteId:-1});
        this.setState({selectedCategoryId:-1});
        this.setState({selectedVehicleId:-1});
        this._dropdown_site_select(0);
        this._dropdown_category_select(0);
        this._dropdown_vehicle_select(0);
        this.setState({queryString:"dashboard_fullscreen.xhtml?token="+this.state.token});
        this.setState({togglePanel:!this.state.togglePanel});
    }
    _dropdown_site_select(idx) {
        this._dropdown_site && this._dropdown_site.select(idx);
    }
    _dropdown_category_select(idx) {
        this._dropdown_category && this._dropdown_category.select(idx);
    }
    _dropdown_vehicle_select(idx) {
        this._dropdown_vehicle && this._dropdown_vehicle.select(idx);
    }
    render (){
        let url = "https://tracking.safee.xyz/";
        // Platform Spesific
        
        return (
            <View style={{ flex: 1 }}>
                <Spinner visible={this.state.loading} textStyle={{color: '#FFF'}} />
                <View style={{flex:1}}>
                    <View style={styles.topbar}>
                        <Image 
                            style={styles.toplogo}
                            source={require('../../assets/logo.png')}
                        />
                        <Button
                            backgroundColor = "white"
                            rounded
                            paddingRight="20"
                            color="black"
                            buttonStyle={styles.menubutton}
                            icon={{
                                name: 'menu',
                                type: 'feather',
                                color:'#333',
                                size:32
                            }}
                            onPress={this.togglePanelHandler}
                            />
                    </View>

                    <View style={[styles.toggleMenu,this.state.togglePanel ? { display:'none',position:'relative' } : ""]}>
                        <ModalDropdown 
                            ref={el => this._dropdown_site = el}
                            default style={styles.dropdown}
                            textStyle={styles.dropdownText}
                            dropdownStyle={styles.dropdownStyle}
                            options={this.state.sitesDropdown}
                            defaultIndex={0}
                            onSelect={this.selectSite.bind(this)}
                            />
                        <ModalDropdown 
                            ref={el => this._dropdown_category = el}
                            default style={styles.dropdown}
                            textStyle={styles.dropdownText}
                            dropdownStyle={styles.dropdownStyle}
                            options={this.state.categoriesDropdown}
                            defaultValue="All Categories"
                            onSelect={this.selectCategory.bind(this)}
                            />
                        <ModalDropdown 
                            ref={el => this._dropdown_vehicle = el}
                            default style={styles.dropdown}
                            textStyle={styles.dropdownText}
                            dropdownStyle={styles.dropdownStyle}
                            options={this.state.vehiclesDropdown}
                            defaultValue="All Vehicles"
                            onSelect={this.selectVehicle.bind(this)}
                            />
                        <View style={[styles.buttonContainer,styles.marginSeperator]}>
                            <Button 
                                style={styles.button}
                                leftIcon={{
                                    name: 'filter',
                                    type: 'feather',
                                }}
                                backgroundColor="#7AC943"
                                rounded
                                title="EXPLORE DASHBOARD"
                                onPress={() => this.filterHandler(this.state.token)} />    
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button 
                                style={styles.button}
                                leftIcon={{
                                    name: 'pie-chart',
                                    type: 'feather',
                                }}
                                backgroundColor="#2AAFEB"
                                rounded
                                title="CLEAR FILTER"
                                onPress={this.clearFilterHandler.bind(this)} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button 
                                style={styles.button}
                                title="LOGOUT"
                                leftIcon={{
                                    name: 'log-out',
                                    type: 'feather',
                                }}
                                marginTop="20"
                                rounded
                                onPress={this.logoutHandler} /> 
                        </View>
                    </View>
                    <WebView
                        style={[styles.webview,!this.state.togglePanel ? { ...Platform.select({ android:{ display:'none'}}),position:'relative' } : ""]}
                        source={{uri: "https://tracking.safee.xyz/"+this.state.queryString}}
                        onLoadStart={() => (this.setState({loading:true}))}
                        onLoad={() => (this.setState({loading:false}))}
                        />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    topbar:{
        ...Platform.select({
            ios:{
                marginTop:20
            }
        }),
        height:52,
        width: "100%",
        backgroundColor:'white',
        paddingLeft:15,
        paddingRight:15,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    menubutton:{
        marginRight:-40
    },
    buttonContainer:{
        marginTop:10,
    },
    button:{
        shadowOffset:{  width: 0,  height: 0,  },
        shadowRadius: 5,
        shadowColor: 'black',
        shadowOffset: { height: 5, width: 0 }
    },
    marginSeperator:{
        marginTop:20,
    },
    toplogo:{
        height:36,
        width:86
    },
    toggleMenu:{
        backgroundColor:'white',
        paddingBottom:15,
        borderBottomColor: '#F0F0F0',
        borderBottomWidth: 1,
        position: 'absolute',
        left:0,
        right:0,
        zIndex:100,
        ...Platform.select({
            ios:{
                top:72
            },
            android:{
                top:52
            }            
        }),
    },
    dropdown:{
        height:44,
        borderRadius:50,
        margin:15,
        borderColor:'#F0F0F0',
        borderWidth:1,
        borderRadius:50,
        paddingLeft:15,
        paddingRight:15,
        paddingTop:5,
        marginBottom:-5,
        paddingBottom:5,
        shadowOffset:{  width: 0,  height: 0,  },
        shadowColor: 'black',
        shadowOpacity: 0.06,
    },
    dropdownText:{
        color:'#333',
        fontSize:15,
        marginTop:6,
        textAlign: 'left'
    },
    dropdownStyle:{
        width:deviceWidth*0.96,
        borderColor:'#F0F0F0',
        height:'auto',
        maxHeight:240,
        borderWidth:1,
        borderRadius:3,
        paddingLeft:15,
        paddingRight:15,
        paddingTop:5,
        paddingBottom:5,
        marginTop:20,
        marginLeft:-24,
        shadowOffset:{  width: 0,  height: 0,  },
        shadowColor: 'black',
        shadowOpacity: 0.06,
    },
    webview: {
      flex: 1,
      width: deviceWidth,
      height: deviceHeight,
      transform: [{'translate':[0,0,1]}] 
    }
  });

export default HomeScreen; 