import React, { Component } from 'react';
import { View, Text, Image, TextInput, StyleSheet, Dimensions, Alert, Platform } from 'react-native';
import { Button } from 'react-native-elements';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
import Spinner from 'react-native-loading-spinner-overlay';
import * as Keychain from 'react-native-keychain';

class AuthScreen extends Component {
    constructor(props){
      super(props);
      this.state = {
          username:"",
          password:"",
          loading:false
      }
      this.loginHandler.bind(this);
    }
    async componentWillMount(){
      // When you have the JWT credentials
      try {
        // Retreive the credentials
        const credentials = await Keychain.getGenericPassword();
        if (credentials) {
          if(credentials.username != undefined && credentials.password != undefined){
            this.goHome();
          }
        } else {
          console.log('No credentials stored');
        }
      } catch (error) {
        console.log('Keychain couldn\'t be accessed!', error);
      } 
        //await Keychain.resetGenericPassword(); 
    }
    async addTokenandGo(username,password){
      await Keychain.setGenericPassword(username,password);
      this.goHome();
    }
    loginHandler = () => {
        this.setState({loading: true});
        this.loginRequestToRemote(this.state.username,this.state.password);
    }
    loginRequestToRemote(username,password){
      let url = "";
      // Platform Spesific
      if (Platform.OS == 'ios') 
        url = "https://tracking.safee.xyz/api/v1/auth/login";
      else
        url = "http://tracking.safee.xyz/api/v1/auth/login";

      if(this.state.username === "" || this.state.password === ""){
        this.setState({loading: false});
        Alert.alert(
          'Warning!',
          "Please fill username and password!",
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: true }
        );
      }else{
        fetch(url, {
              method: 'POST',
              headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                  username: this.state.username,
                  password: this.state.password,
                  fcmToken: "foMYWGYk2L0:APA91bGZ...."
              }),
          }).then((response) => response.json())
          .then((responseJson) => {
            if(responseJson.status == "success"){
              this.setState({loading: false});
              // Add Token and Go
              setTimeout(()=>{
                this.addTokenandGo(this.state.username,this.state.password);
              },100);
            }else{
              Alert.alert(
                'Error',
                responseJson.message,
                [
                  {text: 'OK', onPress: () => this.setState({loading: false})},
                ],
                { cancelable: false }
              );
            }
          })
          .catch((error) => {
            console.error(error);
            this.setState({loading: false});
          });
      }
    }
    goHome() {
      this.props.navigator.push({
        screen: 'safeelite.HomeScreen',
        title: 'Pushed Screen',
        headerLeft: null,
        navigatorStyle: {navBarHidden: true}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
      });
    }
    render (){
        return (
            <View style={styles.container}>
                <Spinner visible={this.state.loading} textStyle={{color: '#FFF'}} />
                <Image 
                  style={styles.logo}
                  source={require('../../assets/logo.png')}
                />
                <Text>
                  Safee Lite
                </Text>
                <TextInput
                  placeholder="Username"
                  underlineColorAndroid='rgba(0,0,0,0)'
                  autoCorrect={false}
                  underlineColorAndroid='transparent'
                  style={styles.input}
                  autoCapitalize = 'none'
                  onChangeText={(text) => this.setState({username:text})}
                  value={this.state.username}
                />
                <TextInput
                  placeholder="Password"
                  underlineColorAndroid='rgba(0,0,0,0)'
                  autoCorrect={false}
                  underlineColorAndroid='transparent'
                  style={styles.input}
                  onChangeText={(text) => this.setState({password:text})}
                  secureTextEntry={true}
                  onSubmitEditing={()=>{this.loginHandler();}}
                  value={this.state.password}
                />
                <View style={styles.button}>
                  <Button 
                    buttonStyle={styles.button}
                    backgroundColor="#7AC943"
                    color="#FFF"
                    margin="0"
                    rounded={true}
                    title="Login"
                    onPress={this.loginHandler} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#FFF'
  },
  input:{
    height:42,
    width:deviceWidth*0.8,
    borderColor:'#F0F0F0',
    borderWidth:1,
    borderRadius:50,
    paddingLeft:15,
    paddingRight:15,
    paddingTop:5,
    paddingBottom:5,
    marginTop:10,
    shadowOffset:{  width: 0,  height: 0,  },
    shadowColor: 'black',
    color:'#333',
    shadowOpacity: 0.06,
  },
  logo:{
    height:80,
    width:189
  },
  button:{
    width:deviceWidth*0.8,
    marginTop:12,
    marginLeft:-10
  }
});

export default AuthScreen;