import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import * as Keychain from 'react-native-keychain';

class SplashScreen extends Component {
    constructor(props){
      super(props);
    }
    async componentWillMount(){
      // When you have the JWT credentials
      try {
        // Retreive the credentials
        const credentials = await Keychain.getGenericPassword();
        if (credentials) {
          if(credentials.username != undefined && credentials.password != undefined){
            this.goHome();
          }
        } else {
          this.goLogin();
          console.log('No credentials stored');
        }
      } catch (error) {
        this.goLogin();
        console.log('Keychain couldn\'t be accessed!', error);
      } 
    }
    goHome() {
      this.props.navigator.push({
        screen: 'safeelite.HomeScreen',
        title: 'Pushed Screen',
        headerLeft: null,
        navigatorStyle: {navBarHidden: true}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
      });
    }
    goLogin(){
        this.props.navigator.push({
            screen: 'safeelite.AuthScreen',
            title: 'Login Screen',
            headerLeft: null,
            navigatorStyle: {navBarHidden: true}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
        });
    }
    render (){
        return (
            <View style={styles.container}>
                <Image 
                style={styles.logo}
                source={require('../../assets/logo.png')}
                />
                <Text>
                Safee Lite
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#FFF'
  },
  logo:{
    height:80,
    width:189
  }
});

export default SplashScreen;